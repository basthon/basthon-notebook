**This project has moved [here](https://forge.apps.education.fr/basthon/basthon-notebook)**.

See [Basthon-Kernel's Readme](https://framagit.org/casatir/basthon-kernel/-/blob/master/README.md#basthon).


    npm install
    npm start
